const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const model = mongoose.model.bind(mongoose);
const ObjectId = Schema.Types.ObjectId;

const userSchema = new Schema({
  account: String,
  password: String,
});

const Users = model('Users', userSchema);

module.exports = { Users };
