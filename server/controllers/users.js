const Model = require('../model');
const { Users } = Model;

const usersController = {
  // 登录验证
  login(req, res) {
    let { account, password } = req.body;
    Users.findOne({ account, password }, (err, user) => {
      if (err) {
        res.json({
          code: 500,
          msg: '服务器错误',
        });
      } else if (!user) {
        res.json({
          code: 404,
          msg: '用户不存在',
        });
      } else {
        res.json({
          code: 200,
          msg: '登录成功',
          data: user,
        });
      }
    });
  },
  // 添加用户
  add(req, res) {
    const newUsers = new Users(req.body);
    console.log(req.body);
    newUsers.save((err, user) => {
      if (err) {
        res.json({
          code: 500,
          msg: '服务器错误',
        });
      } else {
        Users.findOne({ _id: user._id }, (err, user) => {
          res.json({
            code: 200,
            msg: '添加成功',
            data: user,
          });
        });
      }
    });
  },
  // 查询所有用户
  all(req, res) {
    Users.find({}, (err, users) => {
      if (err) {
        res.json({
          code: 500,
          msg: '服务器错误',
        });
      } else {
        res.json({
          code: 200,
          msg: '查询成功',
          data: users,
        });
      }
    });
  },
};

module.exports = usersController;