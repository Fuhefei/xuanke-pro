const express = require('express');
const router = express.Router();
const usersController = require('../controllers/users');


router.post('/login', usersController.login);
router.get('/users', usersController.all);
router.post('/users', usersController.add);

module.exports = router;
